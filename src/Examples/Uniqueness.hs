{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Examples.Uniqueness where

import TH.Defaults.Arguments
import TH.Defaults.Expressions
import TH.Defaults.Signatures

-- Defaults uniqueness analisis
unique
  [d|
    b0 = a0 ? 3

    c0 = (a0 ? 3) + b0

    d0 = 1

    e0 = d0 + 1

    f0 = a0 ? 1

    f = a1 ? (b1 ? 1)

    a2 = x ? 0

    b2 = a2 ? 3

    c2 = a2 + b2

    a3 = sum ([a1, b1, c1, d1, e1, 3] ? 0)

    a4 = let x = 1 in a3 ? x

    a5 = let x = 1 in 1 + (a3 ? (x + 1))

    a6 = let x = 1 in 1 + (let y = 2 in a3 ? (x + y))
    |]

main :: IO () -- THIS IS NEEDED
main = uniqueness_analysis
