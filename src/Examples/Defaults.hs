{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Examples.Defaults where

import TH.Defaults.Arguments
import TH.Defaults.Expressions
import TH.Defaults.Signatures

-- Defaults signatures
signatures [d|
    add :: (Integer ? 0) -> (Integer ? 0) -> Integer
    add = (+)

    and :: (Bool ? True) -> (Bool ? True) -> Bool
    and = (&&)
             |]


-- Defaults in expressions
defaults . arguments $ [d|b0 = a0 ? 3; ; c0 = (a0 ? 3) + b0; ; d0 = 1; ; e0 = d0 + 1; ; f0 = a0 ? 1|]
defaults . arguments $ [d|f = a1 ? (b1 ? 1)|]
defaults . arguments $ [d|a2 = x ? 0; ; b2 = a2 ? 3; ; c2 = a2 + b2|]
defaults . arguments $ [d|a3 = sum ([a1, b1, c1, d1, e1, 3] ? 0)|]
defaults . arguments $ [d| a4 = let x = 1 in (a3 ? x)|]
