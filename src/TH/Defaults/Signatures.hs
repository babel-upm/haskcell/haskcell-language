{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskellQuotes #-}
{-# LANGUAGE TypeOperators #-}

module TH.Defaults.Signatures where

import Language.Haskell.TH

data a ? b = Default

signatures :: Q [Dec] -> Q [Dec]
signatures decs = do
    decs' <- decs
    decs'' <- mapM removeDefaults decs'
    argsDecs <- mapM extractDefaults decs'
    return $ concat argsDecs ++ decs''

removeDefaults :: Dec -> Q Dec
removeDefaults (SigD n t) = do
    t' <- removeDefaultsType t
    return (SigD n t')
removeDefaults decl = return decl

removeDefaultsType :: Type -> Q Type
removeDefaultsType (AppT (AppT op t) d) = do
    op' <- [t|(?)|]
    if op == op'
        then return t
        else do
            t' <- removeDefaultsType t
            d' <- removeDefaultsType d
            return $ AppT (AppT op t') d'
removeDefaultsType (AppT t1 t2) = do
    t1' <- removeDefaultsType t1
    t2' <- removeDefaultsType t2
    return $ AppT t1' t2'
removeDefaultsType t = return t

extractDefaults :: Dec -> Q [Dec]
extractDefaults (SigD n t) = do
    args <- extractDefaultFromType arg_name t
    let renamed_args = zipWith namedArg args [1 ..]
    return renamed_args
  where
    arg_name = mkName $ nameBase n <> "_arg"
    namedArg (SigD arg_name t) num = SigD (mkName $ nameBase arg_name <> "_" <> show num) t
    namedArg (ValD (VarP arg_name) p d) num = ValD (VarP (mkName $ nameBase arg_name <> "_" <> show num)) p d
extractDefaults _ = return []

-- TODO: throw error when (?) nested in (->)
extractDefaultFromType :: Name -> Type -> Q [Dec]
extractDefaultFromType name (AppT (AppT op t) d) = do
    op' <- [t|(?)|]
    t' <- extractDefaultFromType name t
    if op == op'
        then do
            body <- typeToBody d
            return (ValD (VarP name) body [] : t')
        else do
            d' <- extractDefaultFromType name d
            return $ t' ++ d'
extractDefaultFromType name (AppT t1 t2) = do
    t1' <- extractDefaultFromType name t1
    t2' <- extractDefaultFromType name t2
    return $ t1' ++ t2'
extractDefaultFromType _ _ = return []

typeToExp :: Type -> Q Exp
typeToExp (LitT (NumTyLit x)) = return $ LitE (IntegerL x)
typeToExp (PromotedT x) = return $ ConE x
typeToExp (ConT x) = return $ ConE x
typeToExp (AppT x y) = do
    x' <- typeToExp x
    y' <- typeToExp y
    return $ AppE x' y'
typeToExp (LitT (StrTyLit s)) = return $ LitE (StringL s)
typeToExp (VarT name) = do
    var <- lookupValueName (nameBase name)
    case var of
        Just var -> return $ VarE var
        Nothing -> error $ "unknown variable " <> show (nameBase name)
typeToExp x = error $ "unsupported type in (?): " <> show x

typeToBody :: Type -> Q Body
typeToBody t = do
    t' <- typeToExp t
    return $ NormalB t'
