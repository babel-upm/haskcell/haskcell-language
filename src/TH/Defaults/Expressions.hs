{-# LANGUAGE TemplateHaskellQuotes #-}

module TH.Defaults.Expressions where

import qualified Control.Exception as E (SomeException, catch)
import Control.Monad (forM)
import Data.List (groupBy, sortBy)
import Data.Maybe (catMaybes, mapMaybe)
import Language.Haskell.TH

-- (?) :: a -> a -> a
(?) = error "(?) not removed during splice"

defaults :: Q [Dec] -> Q [Dec]
defaults decs = do
  decs' <- decs
  mapM defaultsDec decs'

defaultsExpr :: Exp -> Q Exp
defaultsExpr (InfixE (Just l) op (Just r)) = do
  op' <- [|(?)|]
  l' <- defaultsExpr l
  r' <- defaultsExpr r
  case l' of
    (UnboundVarE _) | op' == op -> defaultsExpr r'
    ListE exps | op' == op -> defaultsExpr $ ListE $ fmap (\e -> InfixE (Just e) op (Just r)) exps
    l | op' == op -> defaultsExpr l
    _ -> return $ InfixE (Just l') op (Just r')
defaultsExpr (InfixE Nothing op (Just r)) = do
  op' <- [|(?)|]
  r' <- defaultsExpr r
  case r' of
    _ | op' == op -> error "missing argument left argument on (?)"
    _ -> return $ InfixE Nothing op (Just r')
defaultsExpr (InfixE (Just l) op Nothing) = do
  op' <- [|(?)|]
  l' <- defaultsExpr l
  case l of
    _ | op' == op -> error "missing argument right argument on (?)"
    _ -> return $ InfixE (Just l') op Nothing
defaultsExpr (UInfixE l op r) = do
  op' <- [|(?)|]
  l' <- defaultsExpr l
  r' <- defaultsExpr r
  case l' of
    (UnboundVarE _) | op' == op -> defaultsExpr r'
    ListE exps -> defaultsExpr $ ListE $ fmap (\e -> InfixE (Just e) op (Just r)) exps
    l | op' == op -> defaultsExpr l
    _ -> return $ InfixE (Just l') op (Just r')
defaultsExpr (AppE e1 e2) = do
  e1' <- defaultsExpr e1
  e2' <- defaultsExpr e2
  return $ AppE e1' e2'
defaultsExpr (AppTypeE e t) = do
  e' <- defaultsExpr e
  return $ AppTypeE e' t
defaultsExpr (ParensE e) = do
  e' <- defaultsExpr e
  return $ ParensE e'
defaultsExpr (LamE pats e) = do
  e' <- defaultsExpr e
  pats' <- mapM defaultsPat pats
  return $ LamE pats' e'
defaultsExpr (TupE maybe_exps) = do
  maybe_exps' <- mapM (mapM defaultsExpr) maybe_exps
  return $ TupE maybe_exps'
defaultsExpr (UnboxedTupE maybe_exps) = do
  maybe_exps' <- mapM (mapM defaultsExpr) maybe_exps
  return $ UnboxedTupE maybe_exps'
defaultsExpr (UnboxedSumE e sum_alt sum_ari) = do
  e' <- defaultsExpr e
  return $ UnboxedSumE e' sum_alt sum_ari
defaultsExpr (CondE e1 e2 e3) = do
  e1' <- defaultsExpr e1
  e2' <- defaultsExpr e2
  e3' <- defaultsExpr e3
  return $ CondE e1' e2' e3'
defaultsExpr (MultiIfE guard_exps) = do
  guard_exps' <- mapM defaultsGuard guard_exps
  return $ MultiIfE guard_exps
defaultsExpr (LetE decs e) = do
  decs' <- mapM defaultsDec decs
  e' <- defaultsExpr e
  return $ LetE decs' e'
defaultsExpr (CaseE e ms) = do
  e' <- defaultsExpr e
  return $ CaseE e' ms
defaultsExpr (DoE m stmts) = do
  stmts' <- mapM defaultsStmt stmts
  return $ DoE m stmts
defaultsExpr (MDoE m stmts) = do
  stmts' <- mapM defaultsStmt stmts
  return $ MDoE m stmts'
defaultsExpr (CompE stmts) = do
  stmts' <- mapM defaultsStmt stmts
  return $ CompE stmts'
defaultsExpr (ListE es) = do
  es' <- mapM defaultsExpr es
  return $ ListE es'
defaultsExpr (SigE e t) = do
  e' <- defaultsExpr e
  return $ SigE e' t
defaultsExpr (RecConE n f_es) = do
  f_es' <- mapM defaultsFieldExp f_es
  return $ RecConE n f_es'
defaultsExpr (RecUpdE e f_es) = do
  e' <- defaultsExpr e
  f_es' <- mapM defaultsFieldExp f_es
  return $ RecUpdE e' f_es'
defaultsExpr (StaticE e) = do
  e' <- defaultsExpr e
  return $ StaticE e'
defaultsExpr exp = return exp

defaultsGuard :: (Guard, Exp) -> Q (Guard, Exp)
defaultsGuard (g, e) = do
  e' <- defaultsExpr e
  return (g, e')

defaultsFieldExp :: FieldExp -> Q FieldExp
defaultsFieldExp (n, e) = do
  e' <- defaultsExpr e
  return (n, e')

defaultsStmt :: Stmt -> Q Stmt
defaultsStmt (BindS p e) = do
  p' <- defaultsPat p
  e' <- defaultsExpr e
  return $ BindS p' e'
defaultsStmt (LetS decs) = do
  decs' <- mapM defaultsDec decs
  return $ LetS decs'
defaultsStmt (NoBindS e) = do
  e' <- defaultsExpr e
  return $ NoBindS e'
defaultsStmt (ParS stmts) = do
  stmts' <- mapM (mapM defaultsStmt) stmts
  return $ ParS stmts'
defaultsStmt (RecS stmts) = do
  stmts' <- mapM defaultsStmt stmts
  return $ RecS stmts'

defaultsDec :: Dec -> Q Dec
defaultsDec (FunD name clauses) = do
  clauses' <- mapM defaultsClauses clauses
  return $ FunD name clauses'
defaultsDec (ValD pat body decs) = do
  body' <- defaultsBody body
  decs' <- mapM defaultsDec decs
  return $ ValD pat body' decs'
defaultsDec d = return d

defaultsClauses :: Clause -> Q Clause
defaultsClauses (Clause pats body decs) = do
  pats' <- mapM defaultsPat pats
  body' <- defaultsBody body
  decs' <- mapM defaultsDec decs
  return $ Clause pats' body' decs'

defaultsPat :: Pat -> Q Pat
defaultsPat = return

defaultsBody :: Body -> Q Body
defaultsBody (NormalB exp) = do
  exp' <- defaultsExpr exp
  return (NormalB exp')
defaultsBody (GuardedB guards) = do
  guards' <- mapM defaultsGuard guards
  return (GuardedB guards')
  where
    defaultsGuard (guard, exp) = do
      exp' <- defaultsExpr exp
      return (guard, exp')

----------------------------------------------------------------------------------------------------

unique :: Q [Dec] -> Q [Dec]
unique decs = do
  decs' <- decs
  uniques <- mapM uniqueDec decs'
  checks <- uniquenessCheck (concat uniques)
  defaults $ return (decs' <> checks)

uniquenessCheck :: [(Name, Exp)] -> Q [Dec]
uniquenessCheck unique = do
  funs <- mapM uniqueFun . groupBy (\(n1, _) (n2, _) -> n1 == n2) . sortBy (\(n1, _) (n2, _) -> compare n1 n2) $ unique
  analysis <- uniquenessAnalysis funs
  let checks = fmap snd funs
  return (analysis : checks)

uniquenessAnalysis :: [(Name, Dec)] -> Q Dec
uniquenessAnalysis names = do
  name <- newName "uniqueness_analysis"
  checks <-
    forM
      names
      ( \(n, FunD unique _) -> do
          let name = LitE (StringL (nameBase n)) in [|let result = $(return $ VarE unique) in E.catch (printResult $(return name) result) (printError $(return name) . handler)|]
      )
  let return_expr = AppE (VarE (mkName "return")) (ConE (mkName "()"))
      do_expr = DoE Nothing (fmap NoBindS (checks <> [return_expr]))
  return $ FunD name [Clause [] (NormalB do_expr) []]

uniqueFun :: [(Name, Exp)] -> Q (Name, Dec)
uniqueFun ((name, e) : xs) = do
  name' <- newName (nameBase name <> "_unique")
  let xs' = ListE $ map snd xs
  check <- [|show (if not (all (== $(return e)) $(return xs')) then error "multiple default values" else $(return e))|]
  return (name, FunD name' [Clause [] (NormalB check) []])

uniqueDec :: Dec -> Q [(Name, Exp)]
uniqueDec (FunD name clauses) = do
  clauses' <- mapM uniqueClauses clauses
  return $ concat clauses'
uniqueDec (ValD _ body decs) = do
  body' <- uniqueBody body
  decs' <- mapM uniqueDec decs
  return $ body' <> concat decs'
uniqueDec dec = return []

uniqueClauses :: Clause -> Q [(Name, Exp)]
uniqueClauses (Clause pats body decs) = do
  pats' <- mapM uniquePat pats
  body' <- uniqueBody body
  decs' <- mapM uniqueDec decs
  return $ concat pats' <> body' <> concat decs'

uniqueExpr :: Exp -> Q [(Name, Exp)]
uniqueExpr = uniqueExpr' []

uniqueExpr' :: [Dec] -> Exp -> Q [(Name, Exp)]
uniqueExpr' decs (InfixE (Just l) op (Just r)) = do
  l' <- uniqueExpr' decs l
  r' <- uniqueExpr' decs r
  op' <- [|(?)|]
  case l of
    (UnboundVarE n) | op' == op -> return $ [(n, InfixE (Just l) op (Just (LetE decs r)))] <> l' <> r'
    (VarE n) | op' == op -> return $ [(n, InfixE (Just l) op (Just (LetE decs r)))] <> l' <> r'
    ListE exprs | op' == op -> do
      let defaults' = fmap (\n -> (n, r)) (mapMaybe getVars exprs)
      return $ defaults' <> l' <> r'
    v | op' == op -> do
      error "unsuported expression: only variable names are supported in the left-hand side of (?)"
    _ -> return $ l' <> r'
  where
    getVars (VarE n) = Just n
    getVars (UnboundVarE n) = Just n
    getVars _ = Nothing
uniqueExpr' decs (InfixE Nothing op (Just r)) = do
  r' <- uniqueExpr' decs r
  op' <- [|(?)|]
  case r' of
    _ | op' == op -> error "missing argument left argument on (?)"
    _ -> return r'
uniqueExpr' decs (InfixE (Just l) op Nothing) = do
  l' <- uniqueExpr' decs l
  op' <- [|(?)|]
  case l' of
    _ | op' == op -> error "missing argument left argument on (?)"
    _ -> return l'
uniqueExpr' decs (AppE e1 e2) = do
  e1' <- uniqueExpr' decs e1
  e2' <- uniqueExpr' decs e2
  return $ e1' ++ e2'
uniqueExpr' decs (AppTypeE e t) = do uniqueExpr' decs e
uniqueExpr' decs (UInfixE l op r) = do
  l' <- uniqueExpr' decs l
  r' <- uniqueExpr' decs r
  op' <- [|(?)|]
  case l of
    (UnboundVarE n) | op' == op -> return $ [(n, InfixE (Just l) op (Just r))] <> l' <> r'
    (VarE n) | op' == op -> return $ [(n, InfixE (Just l) op (Just r))] <> l' <> r'
    ListE exprs -> do
      exprs' <- mapM (uniqueExpr' decs) exprs
      return $ concat exprs' <> l' <> r'
    v | op' == op -> do
      error "unsuported expression: only variable names are supported in the left-hand side of (?)"
    _ -> return $ l' <> r'
uniqueExpr' decs (ParensE e) = do uniqueExpr' decs e
uniqueExpr' decs (LamE pats e) = do uniqueExpr' decs e
uniqueExpr' decs (TupE maybe_exps) = do
  maybe_exps' <- mapM (uniqueExpr' decs) $ catMaybes maybe_exps
  return $ concat maybe_exps'
uniqueExpr' decs (UnboxedTupE maybe_exps) = do
  maybe_exps' <- mapM (uniqueExpr' decs) $ catMaybes maybe_exps
  return $ concat maybe_exps'
uniqueExpr' decs (UnboxedSumE e sum_alt sum_ari) = do uniqueExpr' decs e
uniqueExpr' decs (CondE e1 e2 e3) = do
  e1' <- uniqueExpr' decs e1
  e2' <- uniqueExpr' decs e2
  e3' <- uniqueExpr' decs e3
  return $ e1' ++ e2' ++ e3'
uniqueExpr' decs (MultiIfE guard_exps) = do
  guard_exps' <- mapM uniqueGuard guard_exps
  return $ concat guard_exps'
uniqueExpr' decs1 (LetE decs2 e@(InfixE (Just l) op (Just r))) = do
  op' <- [|(?)|]
  decs' <- mapM uniqueDec decs2
  let decs = decs1 ++ decs2
  e' <- uniqueExpr' decs $ if op' == op then InfixE (Just l) op (Just (LetE decs r)) else e
  return $ concat decs' ++ e'
uniqueExpr' decs1 (LetE decs2 e) = do
  decs' <- mapM uniqueDec decs2
  e' <- uniqueExpr' (decs1 ++ decs2) e
  return $ concat decs' ++ e'
uniqueExpr' decs (CaseE e ms) = do uniqueExpr' decs e
uniqueExpr' decs (DoE m stmts) = do
  stmts' <- mapM uniqueStmt stmts
  return $ concat stmts'
uniqueExpr' decs (MDoE m stmts) = do
  stmts' <- mapM uniqueStmt stmts
  return $ concat stmts'
uniqueExpr' decs (CompE stmts) = do
  stmts' <- mapM uniqueStmt stmts
  return $ concat stmts'
uniqueExpr' decs (ListE es) = do
  es' <- mapM (uniqueExpr' decs) es
  return $ concat es'
uniqueExpr' decs (SigE e t) = do uniqueExpr' decs e
uniqueExpr' decs (RecConE n f_es) = do
  f_es' <- mapM uniqueFieldExp f_es
  return $ concat f_es'
uniqueExpr' decs (RecUpdE e f_es) = do
  e' <- uniqueExpr' decs e
  f_es' <- mapM uniqueFieldExp f_es
  return $ e' <> concat f_es'
uniqueExpr' decs (StaticE e) = do uniqueExpr' decs e
uniqueExpr' decs exp = do
  return []

uniqueGuard :: (Guard, Exp) -> Q [(Name, Exp)]
uniqueGuard (_, e) = uniqueExpr e

uniqueFieldExp :: FieldExp -> Q [(Name, Exp)]
uniqueFieldExp (n, e) = do uniqueExpr e

uniqueStmt :: Stmt -> Q [(Name, Exp)]
uniqueStmt (BindS p e) = do
  p' <- uniquePat p
  e' <- uniqueExpr e
  return $ p' <> e'
uniqueStmt (LetS decs) = do
  decs' <- mapM uniqueDec decs
  return $ concat decs'
uniqueStmt (NoBindS e) = do uniqueExpr e
uniqueStmt (ParS stmts) = do
  stmts' <- mapM (mapM uniqueStmt) stmts
  return $ concat $ concat stmts'
uniqueStmt (RecS stmts) = do
  stmts' <- mapM uniqueStmt stmts
  return $ concat stmts'

uniquePat :: Pat -> Q [(Name, Exp)]
uniquePat pat = return []

uniqueBody :: Body -> Q [(Name, Exp)]
uniqueBody (NormalB exp) = do uniqueExpr exp
uniqueBody (GuardedB guards) = do
  guards' <- mapM uniqueGuard guards
  return $ concat guards'

------------------------------------------------------------------------------------------------------
-- Utils

handler :: E.SomeException -> String
handler = head . lines . show

printResult ref result = putStrLn $ ref ++ " => " ++ result

printError ref result = putStrLn $ ref ++ " =# " ++ result
